import type { Entity } from './entity';
import type { Account } from './account';

export type Debt = {
  id: number;
  title: string;
  date: string;
  cost: number;
  count: number;
  entity: Entity;
  account: Account;
  description: string;
};
