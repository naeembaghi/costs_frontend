import type { User } from './user';

type AccountType = {
  id: number;
  name: string;
};

type Currency = {
  id: number;
  name: string;
  symbol: string;
};

export type Account = {
  name: string;
  id: number;
  accountType: AccountType;
  currency: Currency;
  accountNo: string;
  cardNo: string;
  startAmount: number;
  owner: User;
  description: string;
};
