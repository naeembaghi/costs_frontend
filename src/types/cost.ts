import type { CostType } from './costType';
import type { Account } from './account';
import type { Entity } from './entity';

export type Cost = {
  id: number;
  count: number;
  cost: number;
  description: string;
  date: string;
  type: CostType;
  account: Account;
  entity: Entity;
  secondEntity: Entity;
};
