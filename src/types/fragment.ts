import { DocumentNode } from 'graphql';

export type Fragment = {
  read: DocumentNode;
  create: DocumentNode;
  query: DocumentNode;
};
