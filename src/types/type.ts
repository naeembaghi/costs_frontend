export type Type = {
  id: number;
  title: string;
  parent: Type;
};
