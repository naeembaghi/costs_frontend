export type Food = {
  id: number;
  title: string;
};
