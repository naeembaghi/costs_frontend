export type Meal = {
  id: number;
  title: string;
};
