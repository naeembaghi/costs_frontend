import type { Food } from './food';
import type { Meal } from './meal';

export type DailyMeal = {
  id: number;
  date: string;
  food: Food;
  meal: Meal;
  description: string;
};
