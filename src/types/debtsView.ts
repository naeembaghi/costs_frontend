export type DebtsView = {
  id: number;
  title: string;
  debt: number;
};
