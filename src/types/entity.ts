export type Entity = {
  id: number;
  title: string;
};
