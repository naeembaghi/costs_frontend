import type { Entity } from './entity';
import type { Account } from './account';

export type Payment = {
  id: number;
  title: string;
  date: string;
  amount: number;
  entity: Entity;
  account: Account;
};
