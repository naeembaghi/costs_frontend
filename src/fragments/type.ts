import gql from 'graphql-tag';
import type { Fragment } from 'types';

const TypeBaseFragment = gql`
  fragment TypeBaseFragment on Type {
    title
    parent {
      id
      title
    }
  }
`;

const read = gql`
  fragment TypeReadFragment on Type {
    ...TypeBaseFragment
    id
  }
  ${TypeBaseFragment}
`;

const TypeFragment: Fragment = {
  read,
  create: gql`
    fragment TypeCreateFragment on Type {
      ...TypeBaseFragment
    }
    ${TypeBaseFragment}
  `,
  query: gql`
    query TYPES {
      types {
        ...TypeReadFragment
      }
    }
    ${read}
  `,
};

export default TypeFragment;
