import gql from 'graphql-tag';
import type { Fragment } from 'types';

const CurrencyBaseFragment = gql`
  fragment CurrencyBaseFragment on Currency {
    name
    symbol
  }
`;

const read = gql`
  fragment CurrencyReadFragment on Currency {
    ...CurrencyBaseFragment
    id
  }
  ${CurrencyBaseFragment}
`;

const CurrencyFragment: Fragment = {
  read,
  create: gql`
    mutation CREATE_CURRENCY($currency: CurrencyInput!) {
      createCurrency(currencyInput: $currency) {
        id
      }
    }
  `,
  query: gql`
    query TYPES {
      currencies {
        ...CurrencyReadFragment
      }
    }
    ${read}
  `,
};

export default CurrencyFragment;
