import gql from 'graphql-tag';
import type { Fragment } from 'types';

const CostBaseFragment = gql`
  fragment CostBaseFragment on Cost {
    date
    title
    cost
    count
    description
  }
`;
const read = gql`
  fragment CostReadFragment on Cost {
    ...CostBaseFragment
    id
    type {
      id
      title
    }
    account {
      id
      name
    }
    entity {
      id
      title
    }
    secondEntity {
      id
      title
    }
  }
  ${CostBaseFragment}
`;

const CostFragment: Fragment = {
  read,
  create: gql`
    mutation CREATE_COST($cost: CostInput!) {
      createCost(costInput: $cost) {
        id
      }
    }
  `,
  query: gql`
    query COSTS($offset: Int!, $order: JSON, $limit: Int!) {
      costs(limit: $limit, order: $order, offset: $offset) {
        ...CostReadFragment
      }
    }
    ${read}
  `,
};

export default CostFragment;
