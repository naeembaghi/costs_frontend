import gql from 'graphql-tag';
import type { Fragment } from 'types';

const AccountBaseFragment = gql`
  fragment AccountBaseFragment on Account {
    name
    currency {
      id
      name
      symbol
    }
    accountType {
      id
      name
    }
    owner {
      id
      title
    }
    accountNo
    cardNo
    startAmount
    description
  }
`;

const read = gql`
  fragment AccountReadFragment on Account {
    ...AccountBaseFragment
    id
  }
  ${AccountBaseFragment}
`;

const AccountFragment: Fragment = {
  read,
  create: gql`
    mutation CREATE_ACCOUNT($account: AccountInput!) {
      createAccount(accountInput: $account) {
        id
      }
    }
  `,
  query: gql`
    query ACCOUNTS {
      accounts {
        ...AccountReadFragment
      }
    }
    ${read}
  `,
};

export default AccountFragment;
