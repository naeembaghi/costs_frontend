import gql from 'graphql-tag';
import type { Fragment } from 'types';

const UserBaseFragment = gql`
  fragment UserBaseFragment on User {
    firstName
    lastName
  }
`;

const read = gql`
  fragment UserReadFragment on User {
    ...UserBaseFragment
    id
  }
  ${UserBaseFragment}
`;

const UserFragment: Fragment = {
  read,
  create: gql`
    mutation CREATE_USER($user: UserInput!) {
      createUser(userInput: $user) {
        id
      }
    }
  `,
  query: gql`
    query USERS {
      users {
        ...UserReadFragment
      }
    }
    ${read}
  `,
};

export default UserFragment;
