import gql from 'graphql-tag';
import type { Fragment } from 'types';

const MealBaseFragment = gql`
  fragment MealBaseFragment on Meal {
    title
  }
`;

const read = gql`
  fragment MealReadFragment on Meal {
    ...MealBaseFragment
    id
  }
  ${MealBaseFragment}
`;

const MealFragment: Fragment = {
  read,
  create: gql`
    mutation CREATE_MEAL($meal: MealInput!) {
      createMeal(mealInput: $meal) {
        id
      }
    }
  `,
  query: gql`
    query MEALS {
      meals {
        ...MealReadFragment
      }
    }
    ${read}
  `,
};

export default MealFragment;
