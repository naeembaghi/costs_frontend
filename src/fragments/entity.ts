import gql from 'graphql-tag';
import type { Fragment } from 'types';

const EntityBaseFragment = gql`
  fragment EntityBaseFragment on Entity {
    title
  }
`;

const read = gql`
  fragment EntityReadFragment on Entity {
    ...EntityBaseFragment
    id
  }
  ${EntityBaseFragment}
`;

const EntityFragment: Fragment = {
  read,
  create: gql`
    mutation CREATE_ENTITY($entity: EntityInput!) {
      createEntity(entityInput: $entity) {
        id
      }
    }
  `,
  query: gql`
    query ENTITIES {
      entities {
        ...EntityReadFragment
      }
    }
    ${read}
  `,
};

export default EntityFragment;
