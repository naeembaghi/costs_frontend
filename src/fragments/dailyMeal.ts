import gql from 'graphql-tag';
import type { Fragment } from 'types';

const DailyMealBaseFragment = gql`
  fragment DailyMealBaseFragment on DailyMeal {
    date
    description
  }
`;

const read = gql`
  fragment DailyMealReadFragment on DailyMeal {
    ...DailyMealBaseFragment
    id
    food {
      id
      title
    }
    meal {
      id
      title
    }
  }
  ${DailyMealBaseFragment}
`;

const DailyMealFragment: Fragment = {
  read,
  create: gql`
    mutation CREATE_DAILY_MEAL($dailyMeal: DailyMealInput!) {
      createDailyMeal(dailyMealInput: $dailyMeal) {
        id
      }
    }
  `,
  query: gql`
    query DAILY_MEALS {
      dailyMeals(order: [["date", "DESC"]]) {
        ...DailyMealReadFragment
      }
    }
    ${read}
  `,
};

export default DailyMealFragment;
