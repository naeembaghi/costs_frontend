import gql from 'graphql-tag';
import type { Fragment } from 'types';

const DebtBaseFragment = gql`
  fragment DebtBaseFragment on Debt {
    date
    title
    cost
    count
    description
  }
`;
const read = gql`
  fragment DebtReadFragment on Debt {
    ...DebtBaseFragment
    id
    account {
      id
      name
    }
    entity {
      id
      title
    }
  }
  ${DebtBaseFragment}
`;

const debtFragment: Fragment = {
  read,
  create: gql`
    mutation CREATE_DEBT($debt: DebtInput!) {
      createDebt(debtInput: $debt) {
        id
      }
    }
  `,
  query: gql`
    query DEBTS($offset: Int!, $order: JSON, $limit: Int!) {
      debts(limit: $limit, order: $order, offset: $offset) {
        ...DebtReadFragment
      }
    }
    ${read}
  `,
};

export default debtFragment;
