import gql from 'graphql-tag';
import type { Fragment } from 'types';

const PaymentBaseFragment = gql`
  fragment PaymentBaseFragment on Payment {
    date
    title
    amount
    description
  }
`;
const read = gql`
  fragment PaymentReadFragment on Payment {
    ...PaymentBaseFragment
    id
    account {
      id
      name
    }
    entity {
      id
      title
    }
  }
  ${PaymentBaseFragment}
`;

const PaymentFragment: Fragment = {
  read,
  create: gql`
    mutation CREATE_PAYMENT($payment: PaymentInput!) {
      createPayment(paymentInput: $payment) {
        id
      }
    }
  `,
  query: gql`
    query PAYMENTS($offset: Int!, $order: JSON, $limit: Int!) {
      payments(limit: $limit, order: $order, offset: $offset) {
        ...PaymentReadFragment
      }
    }
    ${read}
  `,
};

export default PaymentFragment;
