import gql from 'graphql-tag';
import type { Fragment } from 'types';

const AccountTypeBaseFragment = gql`
  fragment AccountTypeBaseFragment on AccountType {
    name
  }
`;

const read = gql`
  fragment AccountTypeReadFragment on AccountType {
    ...AccountTypeBaseFragment
    id
  }
  ${AccountTypeBaseFragment}
`;

const AccountTypeFragment: Fragment = {
  read,
  create: gql`
    mutation CREATE_ACCOUNT_TYPE($accountType: AccountTypeInput!) {
      createAccountType(accountTypeInput: $accountType) {
        id
      }
    }
  `,
  query: gql`
    query TYPES {
      accountTypes {
        ...AccountTypeReadFragment
      }
    }
    ${read}
  `,
};

export default AccountTypeFragment;
