import gql from 'graphql-tag';
import type { Fragment } from 'types';

const FoodBaseFragment = gql`
  fragment FoodBaseFragment on Food {
    title
  }
`;

const read = gql`
  fragment FoodReadFragment on Food {
    ...FoodBaseFragment
    id
  }
  ${FoodBaseFragment}
`;

const FoodFragment: Fragment = {
  read,
  create: gql`
    mutation CREATE_FOOD($food: FoodInput!) {
      createFood(foodInput: $food) {
        id
      }
    }
  `,
  query: gql`
    query FOODS {
      foods {
        ...FoodReadFragment
      }
    }
    ${read}
  `,
};

export default FoodFragment;
