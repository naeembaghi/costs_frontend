import {
  ApolloClient,
  InMemoryCache,
  NormalizedCacheObject,
  createHttpLink,
} from '@apollo/client';
import { setContext } from '@apollo/link-context';
import type { Store } from 'redux/types/Store';

export const cache = new InMemoryCache();
const GRAPHQL_ENDPOINT = process.env.REACT_APP_API_URL;

const getGraphqlClient = (store: {
  getState: () => Store;
}): ApolloClient<NormalizedCacheObject> => {
  const authLink = setContext((_, { headers }) => {
    const token = store.getState().session?.token;
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : '',
      },
    };
  });

  const httpLink = createHttpLink({ uri: GRAPHQL_ENDPOINT });
  const link = authLink.concat(httpLink);

  return new ApolloClient({
    cache,
    link,
  });
};

export default getGraphqlClient;
