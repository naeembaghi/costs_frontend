import React, { useState } from 'react';
import moment from 'moment-jalaali';
import useLocalizer from 'localizer';
import gql from 'graphql-tag';
import TextField from '@material-ui/core/TextField';
import DatePicker from '@material-ui/lab/DatePicker';
import { useQuery } from '@apollo/react-hooks';
import { Bar } from 'react-chartjs-2';
import useRandomColor from 'hooks/useRandomColor';
import { LinearProgress } from '@material-ui/core';
import { apollo, numbers } from 'utils';

const COSTS_TYPE_REPORT = gql`
  query COSTS_TYPE_REPORT($firstDate: String, $lastDate: String) {
    costsTypeReport(firstDate: $firstDate, secondDate: $lastDate) {
      sum
      title
    }
  }
`;

const CostsTypeReport = (): React.ReactElement => {
  const { getText } = useLocalizer();
  const [from, setFrom] = useState(moment());
  const [to, setTo] = useState(moment());
  const getRandomColor = useRandomColor();
  moment.loadPersian({ usePersianDigits: false });
  const { data, networkStatus } = useQuery<
    { costsTypeReport: Array<{ title: string; sum: number }> },
    { firstDate: string; lastDate: string }
  >(COSTS_TYPE_REPORT, {
    fetchPolicy: 'no-cache',
    variables: {
      firstDate: numbers.toEnglish(moment(from).format('yyyy-MM-DD')),
      lastDate: numbers.toEnglish(moment(to).format('yyyy-MM-DD')),
    },
    notifyOnNetworkStatusChange: true,
  });
  const bgColors = Array(data?.costsTypeReport?.length)
    .fill(0)
    .map(() => getRandomColor());
  return (
    <div>
      <div className="flex flex-row w-full">
        <div className="ml-4">
          <DatePicker
            label={getText('From')}
            onChange={(newValue): void => setFrom(moment(newValue))}
            value={from}
            clearable={false}
            renderInput={(params): React.ReactElement => (
              // eslint-disable-next-line react/jsx-props-no-spreading
              <TextField {...params} />
            )}
          />
        </div>
        <div className="mr-4">
          <DatePicker
            label={getText('To')}
            onChange={(newValue): void => setTo(moment(newValue))}
            value={to}
            clearable={false}
            renderInput={(params): React.ReactElement => (
              // eslint-disable-next-line react/jsx-props-no-spreading
              <TextField {...params} />
            )}
          />
        </div>
      </div>
      <div className="mt-4">
        {apollo.isLoading(networkStatus) && <LinearProgress />}
        {data && (
          <Bar
            data={{
              datasets: [
                {
                  label: getText('costs-types-report'),
                  data: data.costsTypeReport?.map((each) => each.sum),
                  backgroundColor: bgColors.map((each) => `${each}33`),
                  borderColor: bgColors.map((each) => `${each}aa`),
                  borderWidth: 1,
                },
              ],
              labels: data.costsTypeReport?.map((each) => each.title),
            }}
          />
        )}
      </div>
    </div>
  );
};

export default CostsTypeReport;
