import React from 'react';
import useForm from 'components/form';
import { LOGIN } from 'router';
import { useHistory } from 'react-router';
import useLocalizer from 'localizer';
// import { useMutation } from '@apollo/react-hooks';
// import gql from 'graphql-tag';

const SignUp = (): React.ReactElement => {
  // const [signUp] = useMutation(
  //   gql`
  //     mutation SIGN_UP($user: UserInput!) {
  //       signUp(user: $user) {
  //         id
  //       }
  //     }
  //   `,
  // );

  const { getText } = useLocalizer();
  const history = useHistory();

  const { Form } = useForm({
    fields: [
      { type: 'text', name: 'firstName' },
      { type: 'text', name: 'lastName' },
      { type: 'text', name: 'email' },
      { type: 'text', name: 'username' },
      { type: 'password', name: 'password' },
    ],
    onSubmit: (data) => {},
    defaultValues: {
      firstName: '',
      lastName: '',
      username: '',
      email: '',
      password: '',
    },
    onCancel: (): void => history.push(LOGIN),
  });
  return (
    <div className="flex h-full">
      <div className="flex flex-col p-16 pt-4 m-auto border rounded-lg border-gray">
        <div className="flex flex-col m-auto">
          <div className="mx-auto mt-8 mb-8 text-2xl">{getText('sign-up')}</div>
          <Form />
        </div>
      </div>
    </div>
  );
};

export default SignUp;
