import React, { useEffect } from 'react';
import gql from 'graphql-tag';
import jalaaliMoment from 'moment-jalaali';
import { useQuery } from '@apollo/react-hooks';
import { Bar } from 'react-chartjs-2';
import useRandomColor from 'hooks/useRandomColor';
import useLocalizer from 'localizer';
import { apollo, numbers } from 'utils';
import LinearProgress from '@material-ui/core/LinearProgress';

const COSTS_REPORT = gql`
  query COSTS_REPORT($firstDates: [String], $lastDates: [String]) {
    costsReport(firstDates: $firstDates, secondDates: $lastDates)
  }
`;

let firstDates: Array<string> = [];
let lastDates: Array<string> = [];
let labels: Array<string> = [];

const generateDates = (): void => {
  firstDates = [];
  lastDates = [];
  labels = [];
  const currentDate = jalaaliMoment();
  Array(12)
    .fill(0)
    .forEach((_, idx) => {
      currentDate.startOf('month');
      firstDates.push(numbers.toEnglish(currentDate.format('yyyy-MM-DD')));
      labels.push(currentDate.format('jMMMM'));

      currentDate.endOf('month');
      lastDates.push(numbers.toEnglish(currentDate.format('yyyy-MM-DD')));
      currentDate.add(-1, 'month');
    });
};

const CostsReport = (): React.ReactElement => {
  const { getText } = useLocalizer();
  const getRandomColor = useRandomColor();
  useEffect(() => {
    generateDates();
  }, []);
  const { data, networkStatus } = useQuery<
    { costsReport: Array<number> },
    { firstDates: Array<string>; lastDates: Array<string> }
  >(COSTS_REPORT, {
    fetchPolicy: 'no-cache',
    variables: { firstDates, lastDates },
    notifyOnNetworkStatusChange: true,
  });
  const bgColors = Array(12)
    .fill(0)
    .map(() => getRandomColor());
  return (
    <div className="flex flex-col w-full h-full">
      {apollo.isLoading(networkStatus) && <LinearProgress />}
      <div className="flex-1">
        {data && (
          <Bar
            data={{
              datasets: [
                {
                  label: getText('costs-monthly-report-label'),
                  data: data.costsReport,
                  backgroundColor: bgColors.map((each) => `${each}33`),
                  borderColor: bgColors.map((each) => `${each}aa`),
                  borderWidth: 1,
                },
              ],
              labels,
            }}
          />
        )}
      </div>
    </div>
  );
};

export default CostsReport;
