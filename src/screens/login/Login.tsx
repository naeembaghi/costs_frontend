import React, { useState } from 'react';
import useLocalizer from 'localizer';
import { useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { SIGN_UP } from 'router';
import { useDispatch } from 'react-redux';
import Button from '@material-ui/core/Button';
import { login as loginAction } from 'redux/actions/login/login';
import { useHistory } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';

const LOGIN = gql`
  mutation LOGIN($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      token
    }
  }
`;

const Login = (): React.ReactElement => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(false);
  const history = useHistory();
  const dispatch = useDispatch();

  const { getText } = useLocalizer();
  const [loginMutation] = useMutation(LOGIN);

  const submit = (): void => {
    loginMutation({ variables: { username, password } })
      .then((data) => {
        if (data?.data?.login?.token?.length > 0) {
          dispatch(loginAction(data.data.login.token));
        } else {
          setError(true);
        }
      })
      .catch((_) => {
        setError(true);
      });
  };
  return (
    <div className="flex h-full">
      <div className="container flex flex-col m-auto border rounded-lg border-gray">
        <div className="flex flex-col m-auto">
          <div className="mx-auto mt-8 text-2xl">
            {getText('login-to-system')}
          </div>
          {error && (
            <div className="mx-auto mt-8 text-red-500">
              {getText('invalid-username-or-password')}
            </div>
          )}
          <div className="mx-auto mt-8">
            <TextField
              label={getText('username')}
              value={username}
              onChange={(e): void => setUsername(e.target.value)}
              inputProps={{ style: { textAlign: 'left' } }}
            />
          </div>
          <div className="mx-auto mt-8">
            <TextField
              label={getText('password')}
              type="password"
              value={password}
              onChange={(e): void => setPassword(e.target.value)}
              inputProps={{ style: { textAlign: 'left' } }}
            />
          </div>
          <div className="flex justify-between w-full mx-auto mt-8">
            <Button
              onClick={submit}
              variant="contained"
              color="primary"
              className={`ml-4 px-4 py-2 font-semibold text-blue-700 bg-transparent border
                border-blue-500 rounded hover:bg-blue-500 hover:text-white hover:border-transparent`}
            >
              {getText('login')}
            </Button>
            <Button
              onClick={(): void => history.push(SIGN_UP)}
              variant="contained"
              color="secondary"
              className={`mr-4 px-4 py-2 font-semibold text-blue-700 bg-transparent border
                border-blue-500 rounded hover:bg-blue-500 hover:text-white hover:border-transparent`}
            >
              {getText('sign-up')}
            </Button>
          </div>
        </div>
      </div>
      <style>{`
        .container {
          width: 500px;
          height: 500px;
        }
      `}</style>
    </div>
  );
};

export default Login;
