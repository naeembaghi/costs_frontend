import { useCallback, useMemo } from 'react';
import bundle from './bundle.json';

const useLocalizer = (): { getText: (str: string) => string } => {
  const test: { [key: string]: string } = useMemo(() => bundle, []);
  const getText = useCallback(
    (str: string): string => test[str] || str,
    [test],
  );

  return { getText };
};

export default useLocalizer;
