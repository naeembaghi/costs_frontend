import { NetworkStatus } from '@apollo/client';

const isLoading = (networkStatus: NetworkStatus): boolean =>
  [NetworkStatus.error, NetworkStatus.ready].indexOf(networkStatus) < 0;

const defaultExport = {
  isLoading,
};

export default defaultExport;
