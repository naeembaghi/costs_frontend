import React, { useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import Table from 'components/table';
import useLocalizer from 'localizer';
import type { DebtsView } from 'types';

const DEBTS_VIEW = gql`
  query DEBTS_VIEW($order: JSON) {
    debtsView(order: $order) {
      id
      title
      debt
    }
  }
`;

const DebtsViewComponent = (): React.ReactElement => {
  const [sort, setSort] = useState<[[string, string]]>([['title', 'DESC']]);
  const { data, loading } = useQuery<
    { debtsView: Array<DebtsView> },
    { order: Array<Array<string>> }
  >(DEBTS_VIEW, {
    fetchPolicy: 'no-cache',
    variables: { order: sort },
  });
  const { getText } = useLocalizer();

  const onSort = (sortColumn: string, direction: 'ASC' | 'DESC'): void => {
    setSort([[sortColumn, direction]]);
  };

  return (
    <Table
      columns={[
        { title: getText('title'), key: 'title', sortable: true },
        {
          title: getText('debt'),
          key: 'debt',
          className: 'text-left',
          formatter: ['abs', 'amount', 'number'],
          sortable: true,
        },
        {
          title: getText('status'),
          key: 'status',
          fn: (row): string =>
            row.debt > 0 ? getText('creditor') : getText('debtor'),
          className: (row): string =>
            row.debt > 0 ? 'bg-green-100' : 'bg-red-300',
          sortable: true,
        },
      ]}
      data={data?.debtsView}
      onSort={onSort}
      sort={sort[0]}
      loading={loading}
    />
  );
};

export default DebtsViewComponent;
