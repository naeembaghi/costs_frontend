import React from 'react';
import useLocalizer from 'localizer';
import { useHistory } from 'react-router-dom';
import moment from 'moment-jalaali';
import { DEBTS, ENTITIES, COSTS, DAILY_MEALS } from 'router';
import { useDispatch } from 'react-redux';
import { logout } from 'redux/actions/login/login';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { toggleDrawer } from 'redux/actions/ui/ui';
import { makeStyles } from '@material-ui/styles';
import { ApolloConsumer } from '@apollo/client';

moment.loadPersian({ dialect: 'persian-modern', usePersianDigits: true });

const useStyles = makeStyles({
  button: {
    marginRight: 8,
    marginLeft: 8,
  },
});

const headerButtons = [
  { title: 'meals', link: DAILY_MEALS },
  { title: 'costs', link: COSTS },
  { title: 'debts', link: DEBTS },
  { title: 'entities', link: ENTITIES },
];

const Header = (): React.ReactElement => {
  const dispatch = useDispatch();
  const { getText } = useLocalizer();
  const history = useHistory();
  const classes = useStyles();
  return (
    <ApolloConsumer>
      {(client): React.ReactElement => (
        <AppBar>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              aria-label="menu"
              onClick={(): void => {
                dispatch(toggleDrawer());
              }}
            >
              <MenuIcon />
            </IconButton>
            <Button
              onClick={(): void => history.push('/')}
              className={classes.button}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                className="w-10 h-10 p-2 text-white bg-indigo-500 rounded-full"
                viewBox="0 0 24 24"
              >
                <path d="M12 2L2 7l10 5 10-5-10-5zM2 17l10 5 10-5M2 12l10 5 10-5" />
              </svg>
              <span className="mr-3 text-xl">
                {getText('application-title')}
              </span>
            </Button>
            <div className="mx-auto">{moment().format('jYYYY/jMM/jDD')}</div>
            {headerButtons.map((each) => (
              <Button
                key={each.title}
                onClick={(): void => history.push(each.link)}
              >
                {getText(each.title)}
              </Button>
            ))}
            <Button
              onClick={(): void => {
                dispatch(logout());
                client.clearStore();
              }}
              color="secondary"
            >
              {getText('logout')}
            </Button>
          </Toolbar>
        </AppBar>
      )}
    </ApolloConsumer>
  );
};

export default Header;
