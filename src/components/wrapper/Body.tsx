import React from 'react';

type Props = {
  children: React.ReactNode;
};

const Body = ({ children }: Props): React.ReactElement => (
  <div className="flex-1 p-5 overflow-y-auto">{children}</div>
);

export default Body;
