import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Header from './Header';
import Body from './Body';
import RightMenu from './RightMenu';

type Props = {
  children: React.ReactNode;
};

const Wrapper = ({ children }: Props): React.ReactElement => (
  <BrowserRouter>
    <div className="flex h-full">
      <Header />
      <div className="flex flex-1 mt-16">
        <RightMenu />
        <Body>{children}</Body>
      </div>
    </div>
  </BrowserRouter>
);

export default Wrapper;
