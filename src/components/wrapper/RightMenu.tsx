import React from 'react';
import { useHistory } from 'react-router-dom';
import useLocalizer from 'localizer';
import Drawer from '@material-ui/core/Drawer';
import { toggleDrawer } from 'redux/actions/ui/ui';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {
  ENTITIES,
  ACCOUNTS,
  MEALS,
  DAILY_MEALS,
  COSTS,
  TYPES,
  DEBTS_VIEW,
  DEBTS,
  FOODS,
  PAYMENTS,
  COSTS_REPORT,
  COSTS_TYPES_REPORT,
} from 'router';
import { useSelector, useDispatch } from 'react-redux';

import type { Store } from 'redux/types/Store';

const items = [
  { name: 'accounts', link: ACCOUNTS },
  { name: 'meals', link: MEALS },
  { name: 'daily-meals', link: DAILY_MEALS },
  { name: 'foods', link: FOODS },
  { name: 'entities', link: ENTITIES },
  { name: 'costs', link: COSTS },
  { name: 'cost-types', link: TYPES },
  { name: 'debts', link: DEBTS },
  { name: 'debts-collection', link: DEBTS_VIEW },
  { name: 'payments', link: PAYMENTS },
  { name: 'costs-report', link: COSTS_REPORT },
  { name: 'costs-type-report', link: COSTS_TYPES_REPORT },
];

const RightMenu = (): React.ReactElement => {
  const showDrawer = useSelector((store: Store) => store?.ui?.showDrawer);
  const dispatch = useDispatch();
  const { getText } = useLocalizer();
  const history = useHistory();
  return (
    <>
      <Drawer
        anchor="right"
        open={showDrawer}
        onClose={(): void => {
          dispatch(toggleDrawer());
        }}
      >
        <List>
          {items.map((each) => (
            <ListItem
              button
              key={each.name}
              onClick={(): void => history.push(each.link)}
            >
              <ListItemText>{getText(each.name)}</ListItemText>
            </ListItem>
          ))}
        </List>
      </Drawer>
    </>
  );
};

export default RightMenu;
