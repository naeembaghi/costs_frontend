import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import Table from 'components/table';
import useLocalizer from 'localizer';
import { typeFragment } from 'fragments';

const Types = (): React.ReactElement => {
  const { data, loading } = useQuery(typeFragment.query);
  const { getText } = useLocalizer();
  return (
    <Table
      columns={[
        { title: getText('id'), key: 'id', formatter: ['number'] },
        { title: getText('title'), key: 'title' },
        { title: getText('parent'), key: 'parent.title' },
      ]}
      data={data?.types}
      loading={loading}
    />
  );
};

export default Types;
