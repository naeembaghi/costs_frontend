import React from 'react';
import { Modal } from 'react-responsive-modal';

type Props = {
  children: React.ReactNode;
  title: string;
  open: boolean;
  onClose: () => void;
};

const Dialog = ({
  open,
  children,
  title,
  onClose,
}: Props): React.ReactElement => (
  <Modal
    open={open}
    onClose={onClose}
    classNames={{
      closeButton: 'left-0 right-auto ml-4 focus:outline-none',
      closeIcon: 'border rounded-full border-gray-500 p-1 hover:bg-gray-200',
      modal: 'w-1/3',
    }}
    animationDuration={300}
  >
    <header className="flex items-center">
      <span className="ml-8">{title}</span>
    </header>
    <div className="py-8">{children}</div>
  </Modal>
);

export default Dialog;
