import React, { useState } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import Table from 'components/table';
import useLocalizer from 'localizer';
import type { Payment } from 'types';
import { paymentFragment } from 'fragments';
import Dialog from 'components/dialog';
import Button from '@material-ui/core/Button';
import jalaaliMoment from 'moment-jalaali';
import useForm from 'components/form';
import { numbers } from 'utils';

const PaymentComponent = (): React.ReactElement => {
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(25);
  const [isOpen, setIsOpen] = useState(false);
  const [createPayment] = useMutation(paymentFragment.create);
  const [sort, setSort] = useState<[[string, string]]>([['date', 'DESC']]);
  const { data, loading, refetch } = useQuery<
    { payments: Array<Payment> },
    { offset: number; order: Array<Array<string>>; limit: number }
  >(paymentFragment.query, {
    variables: { offset: page * pageSize, order: sort, limit: pageSize },
  });
  const { getText } = useLocalizer();
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const onSubmit = (formData: any): void => {
    createPayment({
      variables: {
        payment: {
          title: formData.title,
          amount: Number(formData.amount),
          entity: {
            id:
              typeof formData.entity === 'string'
                ? formData.entity
                : formData.entity.id,
          },
          account: {
            id:
              typeof formData.account === 'string'
                ? formData.account
                : formData.account.id,
          },
          description: formData.description,
          date: numbers.toEnglish(
            jalaaliMoment(formData.date).format('yyyy-MM-DD'),
          ),
        },
      },
    }).then(() => {
      setIsOpen(false);
      refetch();
    });
  };
  const { Form } = useForm({
    fields: [
      { type: 'text', name: 'title' },
      { type: 'number', name: 'amount' },
      { type: 'date', name: 'date', timePicker: false },
      { type: 'textarea', name: 'description' },
      { type: 'account-autocomplete', name: 'account' },
      { type: 'entity-autocomplete', name: 'entity' },
    ],
    onSubmit,
    defaultValues: {
      name: '',
      amount: '1',
      date: jalaaliMoment(),
      description: '',
      account: '4',
      entity: '1',
    },
  });

  const onSort = (sortColumn: string, direction: 'ASC' | 'DESC'): void => {
    setSort([[sortColumn, direction]]);
  };

  return (
    <>
      <Dialog
        title={getText('add')}
        open={isOpen}
        onClose={(): void => setIsOpen(false)}
      >
        <Form />
      </Dialog>
      <div className="flex items-center justify-end w-full h-10 mb-2">
        <Button variant="outlined" onClick={(): void => setIsOpen(true)}>
          {getText('add')}
        </Button>
      </div>
      <Table
        columns={[
          { title: getText('title'), key: 'title', sortable: true },
          {
            title: getText('amount'),
            key: 'amount',
            className: 'text-left',
            formatter: ['abs', 'amount', 'number'],
            sortable: true,
          },
          {
            title: getText('date'),
            key: 'date',
            className: 'text-center',
            formatter: ['date', 'number'],
            sortable: true,
          },
          { title: getText('entity'), key: 'entity.title' },
          { title: getText('account'), key: 'account.name' },
          {
            title: getText('status'),
            key: 'status',
            fn: (row): string =>
              row.amount > 0 ? getText('creditor') : getText('debtor'),
            className: (row): string =>
              row.amount > 0 ? 'bg-green-100' : 'bg-red-300',
          },
        ]}
        data={data?.payments}
        onSort={onSort}
        sort={sort[0]}
        pagination={{
          page,
          pageSize,
          onPageChange: (_, newPage): void => setPage(newPage),
          onPageSizeChange: (event): void =>
            setPageSize(Number(event.target.value)),
        }}
        loading={loading}
        hasAddButton
        onEdit={(): void => {}}
        onDelete={(): void => {}}
      />
    </>
  );
};

export default PaymentComponent;
