import React, { useState } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import Table from 'components/table';
import Button from '@material-ui/core/Button';
import useLocalizer from 'localizer';
import { accountFragment } from 'fragments';
import Dialog from 'components/dialog';
import useForm from 'components/form';

const Accounts = (): React.ReactElement => {
  const [isOpen, setIsOpen] = useState(false);
  const { data, loading, refetch } = useQuery(accountFragment.query);
  const [createAccount] = useMutation(accountFragment.create);
  const { getText } = useLocalizer();

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const onSubmit = (formData: any): void => {
    createAccount({
      variables: {
        account: {
          name: formData.name,
          accountNo: formData.accountNo,
          cardNo: formData.cardNo,
          startAmount: Number(formData.startAmount),
          description: formData.description,
          accountType: {
            id:
              typeof formData.type === 'string'
                ? formData.type
                : formData.type.id,
          },
          currency: {
            id:
              typeof formData.currency === 'string'
                ? formData.currency
                : formData.currency.id,
          },
          owner: {
            id:
              typeof formData.owner === 'string'
                ? formData.owner
                : formData.owner.id,
          },
        },
      },
    }).then(() => {
      setIsOpen(false);
      refetch();
    });
  };

  const { Form } = useForm({
    fields: [
      { type: 'text', name: 'name' },
      { type: 'text', name: 'accountNo' },
      { type: 'text', name: 'cardNo' },
      { type: 'number', name: 'startAmount' },
      { type: 'textarea', name: 'description' },
      { type: 'account-type-autocomplete', name: 'type' },
      { type: 'currency-autocomplete', name: 'currency' },
      { type: 'entity-autocomplete', name: 'owner' },
    ],
    onSubmit,
    defaultValues: {
      name: '',
      accountNo: '',
      cardNo: '',
      startAmount: '0',
      description: '',
      type: '1',
      owner: '1',
      currency: '1',
    },
  });

  return (
    <>
      <Dialog
        title={getText('add')}
        open={isOpen}
        onClose={(): void => setIsOpen(false)}
      >
        <Form />
      </Dialog>
      <div className="flex justify-end w-full h-10 mb-2">
        <Button variant="outlined" onClick={(): void => setIsOpen(true)}>
          {getText('add')}
        </Button>
      </div>
      <Table
        columns={[
          { title: getText('id'), key: 'id', formatter: ['number'] },
          { title: getText('account-no'), key: 'accountNo' },
          { title: getText('card-no'), key: 'cardNo' },
          { title: getText('account-type'), key: 'accountType.name' },
          { title: getText('currency'), key: 'currency.name' },
          { title: getText('currency-symbol'), key: 'currency.symbol' },
          { title: getText('description'), key: 'description' },
          { title: getText('account-owner'), key: 'owner.title' },
        ]}
        data={data?.accounts}
        loading={loading}
        onEdit={(): void => {}}
        onDelete={(): void => {}}
      />
    </>
  );
};

export default Accounts;
