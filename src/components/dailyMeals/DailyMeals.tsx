import React, { useState } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import Table from 'components/table';
import useLocalizer from 'localizer';
import { dailyMealFragment } from 'fragments';
import Button from '@material-ui/core/Button';
import jalaaliMoment from 'moment-jalaali';
import Dialog from 'components/dialog';
import useForm from 'components/form';
import { numbers } from 'utils';

const DailyMeals = (): React.ReactElement => {
  const { data, loading, refetch } = useQuery(dailyMealFragment.query);
  const [isOpen, setIsOpen] = useState(false);
  const [createDailyMeal] = useMutation(dailyMealFragment.create);
  const { getText } = useLocalizer();
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const onSubmit = (formData: any): void => {
    createDailyMeal({
      variables: {
        dailyMeal: {
          description: formData.description,
          food: {
            id:
              typeof formData.food === 'string'
                ? formData.food
                : formData.food.id,
          },
          meal: {
            id:
              typeof formData.meal === 'string'
                ? formData.meal
                : formData.meal.id,
          },
          date: numbers.toEnglish(
            jalaaliMoment(formData.date).format('yyyy-MM-DD'),
          ),
        },
      },
    }).then(() => {
      setIsOpen(false);
      refetch();
    });
  };
  const { Form } = useForm({
    fields: [
      { type: 'textarea', name: 'description' },
      { type: 'food-autocomplete', name: 'food' },
      { type: 'meal-autocomplete', name: 'meal' },
      { type: 'date', name: 'date', timePicker: false },
    ],
    onSubmit,
    defaultValues: {
      description: '',
      date: jalaaliMoment(),
      meal: '2',
      food: '2',
    },
  });
  return (
    <>
      <Dialog
        title={getText('add')}
        open={isOpen}
        onClose={(): void => setIsOpen(false)}
      >
        <Form />
      </Dialog>
      <div className="flex justify-end w-full h-10 mb-2">
        <Button variant="outlined" onClick={(): void => setIsOpen(true)}>
          {getText('add')}
        </Button>
      </div>
      <Table
        columns={[
          {
            title: getText('date'),
            key: 'date',
            formatter: ['date', 'number'],
            className: 'text-center',
          },
          { title: getText('daily-meal'), key: 'meal.title' },
          { title: getText('food'), key: 'food.title' },
        ]}
        data={data?.dailyMeals}
        loading={loading}
      />
    </>
  );
};

export default DailyMeals;
