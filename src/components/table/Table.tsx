import React, { useCallback } from 'react';
import get from 'lodash.get';
import moment from 'moment-jalaali';
import { numbers } from 'utils';
import { AiFillCaretDown, AiFillCaretUp } from 'react-icons/ai';
import useLocalizer from 'localizer';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import LinearProgress from '@material-ui/core/LinearProgress';
import TableContainer from '@material-ui/core/TableContainer';

moment.loadPersian({ dialect: 'persian-modern', usePersianDigits: true });

type Props<T> = {
  columns: Array<{
    key: string;
    title: string;
    className?: string | ((row: T) => string);
    formatter?: Array<'date' | 'number' | 'amount' | 'abs'>;
    fn?: (each: T) => string | number;
    sortable?: boolean;
  }>;
  data?: Array<T>;
  onSort?: (sortColumn: string, direction: 'ASC' | 'DESC') => void;
  sort?: [string, string];
  pagination?: {
    page: number;
    pageSize: number;
    onPageChange: (
      event: React.MouseEvent<HTMLButtonElement, MouseEvent> | null,
      newPage: number,
    ) => void;
    onPageSizeChange: React.ChangeEventHandler<
      HTMLTextAreaElement | HTMLInputElement
    >;
  };
  loading: boolean;
  onEdit?: (id: number) => void;
  onDelete?: (id: number) => void;
  hasAddButton?: boolean;
};

const TableComponent = <T extends { id: number }>({
  columns,
  data,
  onSort,
  sort,
  pagination,
  loading,
  onEdit,
  onDelete,
  hasAddButton,
}: Props<T>): React.ReactElement => {
  const { getText } = useLocalizer();
  const format = useCallback(
    (
      columnData: string,
      formatter?: Array<'date' | 'number' | 'amount' | 'abs'>,
    ): string => {
      if (!formatter) return columnData;
      let result = columnData;
      formatter.forEach((each) => {
        switch (each) {
          case 'date':
            result = moment(result).format('jYYYY-jMM-jDD');
            break;
          case 'number':
            result = numbers.toPersian(result) ?? '';
            break;
          case 'amount':
            result = Number(result).toLocaleString();
            break;
          case 'abs':
            result = `${Math.abs(Number(result))}`;
            break;
          default:
            break;
        }
      });
      return result;
    },
    [],
  );

  const headersHeight = 64;
  const innerContainersPadding = 40;
  let height = window.innerHeight - (headersHeight + innerContainersPadding);
  if (pagination) height -= 53;
  if (hasAddButton) height -= 48;

  return (
    <>
      <TableContainer sx={{ maxHeight: height }}>
        <Table stickyHeader className="w-full table-auto">
          <TableHead>
            <TableRow className="h-12">
              {columns.map((each) => (
                <TableCell className="whitespace-nowrap" key={each.key}>
                  <div className="flex items-center">
                    {each.title}
                    {each.sortable ? (
                      <>
                        <div className="mr-2">
                          <AiFillCaretUp
                            className={
                              sort && sort[0] === each.key && sort[1] === 'ASC'
                                ? 'text-green-300'
                                : ''
                            }
                            onClick={(): void =>
                              onSort && onSort(each.key, 'ASC')
                            }
                          />
                        </div>
                        <div className="mr-px">
                          <AiFillCaretDown
                            className={
                              sort && sort[0] === each.key && sort[1] === 'DESC'
                                ? 'text-green-300'
                                : ''
                            }
                            onClick={(): void =>
                              onSort && onSort(each.key, 'DESC')
                            }
                          />
                        </div>
                      </>
                    ) : null}
                  </div>
                </TableCell>
              ))}
              {(onEdit || onDelete) && <TableCell />}
            </TableRow>
          </TableHead>
          <TableBody>
            {loading && (
              <TableRow>
                <td colSpan={columns.length + (onEdit || onDelete ? 1 : 0)}>
                  <LinearProgress />
                </td>
              </TableRow>
            )}
            {data?.map((row) => (
              <TableRow key={row.id} className="even:bg-gray-50">
                {columns.map((column) => (
                  <TableCell
                    className={
                      typeof column.className === 'function'
                        ? column.className(row)
                        : column.className
                    }
                    key={column.key}
                  >
                    {format(
                      column.fn ? column.fn(row) : get(row, column.key),
                      column.formatter,
                    )}
                  </TableCell>
                ))}
                {(onEdit || onDelete) && (
                  <TableCell>
                    <div className="flex">
                      {onEdit && (
                        <IconButton
                          aria-label="delete"
                          size="small"
                          onClick={(): void => onEdit(row.id)}
                        >
                          <EditIcon />
                        </IconButton>
                      )}
                      {onDelete && (
                        <IconButton
                          aria-label="delete"
                          size="small"
                          onClick={(): void => onDelete(row.id)}
                        >
                          <DeleteIcon />
                        </IconButton>
                      )}
                    </div>
                  </TableCell>
                )}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      {pagination && (
        <table className="w-full">
          <tbody>
            <tr>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25, 50]}
                count={100}
                rowsPerPage={pagination.pageSize}
                onRowsPerPageChange={pagination.onPageSizeChange}
                labelDisplayedRows={({ from, to, count }): string =>
                  `${numbers.toPersian(from)} - ${numbers.toPersian(
                    to,
                  )} ${getText('of')} ${numbers.toPersian(count)}`
                }
                labelRowsPerPage={`${getText('rows-per-page')}: `}
                page={pagination.page}
                onPageChange={pagination.onPageChange}
              />
            </tr>
          </tbody>
        </table>
      )}
    </>
  );
};

TableComponent.defaultProps = {
  onSort: undefined,
  sort: undefined,
  pagination: undefined,
  data: undefined,
  onEdit: undefined,
  onDelete: undefined,
  hasAddButton: undefined,
};

export default TableComponent;
