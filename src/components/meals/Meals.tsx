import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import Table from 'components/table';
import useLocalizer from 'localizer';
import { mealFragment } from 'fragments';

const Meals = (): React.ReactElement => {
  const { loading, data } = useQuery(mealFragment.query);
  const { getText } = useLocalizer();
  return (
    <Table
      data={data?.meals}
      columns={[{ key: 'title', title: getText('title') }]}
      loading={loading}
    />
  );
};

export default Meals;
