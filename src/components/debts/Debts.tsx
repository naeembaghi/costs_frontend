import React, { useState } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import Table from 'components/table';
import useLocalizer from 'localizer';
import type { Debt } from 'types';
import { debtFragment } from 'fragments';
import Button from '@material-ui/core/Button';
import Dialog from 'components/dialog';
import jalaaliMoment from 'moment-jalaali';
import useForm from 'components/form';
import { numbers, apollo } from 'utils';

const DebtComponent = (): React.ReactElement => {
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(25);
  const [createDebt] = useMutation(debtFragment.create);
  const [isOpen, setIsOpen] = useState(false);
  const [sort, setSort] = useState<[[string, string]]>([['date', 'DESC']]);
  const { data, refetch, networkStatus } = useQuery<
    { debts: Array<Debt> },
    { offset: number; order: Array<Array<string>>; limit: number }
  >(debtFragment.query, {
    variables: { offset: page * pageSize, order: sort, limit: pageSize },
    notifyOnNetworkStatusChange: true,
  });
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const onSubmit = (formData: any): void => {
    createDebt({
      variables: {
        debt: {
          title: formData.title,
          count: Number(formData.count),
          cost: Number(formData.fee),
          entity: {
            id:
              typeof formData.entity === 'string'
                ? formData.entity
                : formData.entity.id,
          },
          account: {
            id:
              typeof formData.account === 'string'
                ? formData.account
                : formData.account.id,
          },
          description: formData.description,
          date: numbers.toEnglish(formData.date.format('yyyy-MM-DD')),
        },
      },
    }).then(() => {
      setIsOpen(false);
      refetch();
    });
  };
  const { Form } = useForm({
    fields: [
      { type: 'text', name: 'title' },
      { type: 'number', name: 'fee' },
      { type: 'number', name: 'count' },
      { type: 'date', name: 'date', timePicker: false },
      { type: 'textarea', name: 'description' },
      { type: 'account-autocomplete', name: 'account' },
      { type: 'entity-autocomplete', name: 'entity' },
    ],
    onSubmit,
    defaultValues: {
      name: '',
      fee: 0,
      count: '1',
      date: jalaaliMoment(),
      description: '',
      account: '4',
      entity: '1',
    },
  });

  const onSort = (sortColumn: string, direction: 'ASC' | 'DESC'): void => {
    setSort([[sortColumn, direction]]);
  };

  const { getText } = useLocalizer();

  return (
    <>
      <Dialog
        title={getText('add')}
        open={isOpen}
        onClose={(): void => setIsOpen(false)}
      >
        <Form />
      </Dialog>
      <div className="flex items-center justify-end w-full h-10 mb-2">
        <Button variant="outlined" onClick={(): void => setIsOpen(true)}>
          {getText('add')}
        </Button>
      </div>
      <Table
        columns={[
          { title: getText('title'), key: 'title', sortable: true },
          {
            title: getText('fee'),
            key: 'cost',
            className: 'text-left',
            formatter: ['abs', 'amount', 'number'],
            sortable: true,
          },
          {
            title: getText('count'),
            key: 'count',
            className: 'text-left',
            formatter: ['abs', 'amount', 'number'],
            sortable: true,
          },
          {
            title: getText('total-amount'),
            key: 'total-amount',
            className: 'text-left',
            formatter: ['abs', 'amount', 'number'],
            fn: (each): number => each.cost * each.count,
          },
          {
            title: getText('date'),
            key: 'date',
            className: 'text-center',
            formatter: ['date', 'number'],
            sortable: true,
          },
          { title: getText('description'), key: 'description', sortable: true },
          { title: getText('entity'), key: 'entity.title' },
          { title: getText('account'), key: 'account.name' },
          {
            title: getText('status'),
            key: 'status',
            fn: (row): string =>
              row.cost > 0 && row.count > 0
                ? getText('creditor')
                : getText('debtor'),
            className: (row): string =>
              row.cost > 0 && row.count > 0 ? 'bg-green-100' : 'bg-red-300',
          },
        ]}
        data={data?.debts}
        onSort={onSort}
        sort={sort[0]}
        pagination={{
          page,
          pageSize,
          onPageChange: (_, newPage): void => setPage(newPage),
          onPageSizeChange: (event): void =>
            setPageSize(Number(event.target.value)),
        }}
        loading={apollo.isLoading(networkStatus)}
        hasAddButton
      />
    </>
  );
};

export default DebtComponent;
