import React, { useState } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import Table from 'components/table';
import useLocalizer from 'localizer';
import Button from '@material-ui/core/Button';
import { foodFragment } from 'fragments';
import Dialog from 'components/dialog';
import useForm from 'components/form';

const Foods = (): React.ReactElement => {
  const { data, loading, refetch } = useQuery(foodFragment.query);
  const [isOpen, setIsOpen] = useState(false);
  const [createFood] = useMutation(foodFragment.create);
  const { getText } = useLocalizer();
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const onSubmit = (formData: any): void => {
    createFood({
      variables: {
        food: {
          title: formData.title,
        },
      },
    }).then(() => {
      setIsOpen(false);
      refetch();
    });
  };
  const { Form } = useForm({
    fields: [{ type: 'text', name: 'title' }],
    onSubmit,
    defaultValues: {
      title: '',
    },
  });
  return (
    <>
      <Dialog
        title={getText('add')}
        open={isOpen}
        onClose={(): void => setIsOpen(false)}
      >
        <Form />
      </Dialog>
      <div className="flex justify-end w-full h-10 mb-2">
        <Button variant="outlined" onClick={(): void => setIsOpen(true)}>
          {getText('add')}
        </Button>
      </div>
      <Table
        columns={[{ title: getText('title'), key: 'title' }]}
        data={data?.foods}
        loading={loading}
      />
    </>
  );
};

export default Foods;
