import React, { useState, useMemo } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import Table from 'components/table';
import Button from '@material-ui/core/Button';
import useLocalizer from 'localizer';
import jalaaliMoment from 'moment-jalaali';
import type { Cost } from 'types';
import { costFragment } from 'fragments';
import Dialog from 'components/dialog';
import { numbers, apollo } from 'utils';
import useForm from 'components/form';

const Costs = (): React.ReactElement => {
  const [page, setPage] = useState(0);
  const [isOpen, setIsOpen] = useState(false);
  const [editId, setEditId] = useState<number | undefined>(undefined);
  const [pageSize, setPageSize] = useState(25);
  const [sort, setSort] = useState<[[string, string]]>([['date', 'DESC']]);
  const { getText } = useLocalizer();

  const [createCost] = useMutation(costFragment.create);

  const { data, refetch, networkStatus } = useQuery<
    { costs: Array<Cost> },
    { offset: number; order: Array<Array<string>>; limit: number }
  >(costFragment.query, {
    variables: { offset: page * pageSize, order: sort, limit: pageSize },
    notifyOnNetworkStatusChange: true,
  });

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const onSubmit = (formData: any): void => {
    createCost({
      variables: {
        cost: {
          title: formData.title,
          count: Number(formData.count),
          cost: Number(formData.cost),
          entity: {
            id:
              typeof formData.entity === 'string'
                ? formData.entity
                : formData.entity.id,
          },
          account: {
            id:
              typeof formData.account === 'string'
                ? formData.account
                : formData.account.id,
          },
          type: {
            id:
              typeof formData.type === 'string'
                ? formData.type
                : formData.type.id,
          },
          description: formData.description,
          date: numbers.toEnglish(
            jalaaliMoment(formData.date).format('yyyy-MM-DD'),
          ),
          secondEntity: formData.secondEntity
            ? {
                id: formData.secondEntity.id,
              }
            : null,
        },
      },
    }).then(() => {
      setIsOpen(false);
      refetch();
    });
  };
  const { Form } = useForm({
    fields: [
      { type: 'text', name: 'title' },
      { type: 'number', name: 'cost' },
      { type: 'number', name: 'count' },
      { type: 'date', name: 'date', timePicker: false },
      { type: 'cost-type-autocomplete', name: 'type' },
      { type: 'textarea', name: 'description' },
      { type: 'account-autocomplete', name: 'account' },
      { type: 'entity-autocomplete', name: 'entity' },
      {
        type: 'entity-autocomplete',
        name: 'second-entity',
      },
    ],
    onSubmit,
    defaultValues: {
      title: '',
      cost: 0,
      count: '1',
      date: jalaaliMoment(),
      type: '1',
      description: '',
      account: '4',
      entity: '1',
      secondEntity: null,
    },
  });

  const onSort = (sortColumn: string, direction: 'ASC' | 'DESC'): void => {
    setSort([[sortColumn, direction]]);
  };

  const editableItem = useMemo(() => {
    if (!editId) return undefined;
    return data?.costs?.find((each) => each.id === editId);
  }, [editId, data?.costs]);

  return (
    <>
      <Dialog
        title={getText('add')}
        open={isOpen}
        onClose={(): void => setIsOpen(false)}
      >
        <Form model={editableItem} />
      </Dialog>
      <div className="flex items-center justify-end w-full h-10 mb-2">
        <Button variant="outlined" onClick={(): void => setIsOpen(true)}>
          {getText('add')}
        </Button>
      </div>
      <Table
        hasAddButton
        columns={[
          { title: getText('title'), key: 'title', sortable: true },
          {
            title: getText('fee'),
            key: 'cost',
            className: 'text-left',
            formatter: ['amount', 'number'],
            sortable: true,
          },
          {
            title: getText('count'),
            key: 'count',
            className: 'text-left',
            formatter: ['number'],
            sortable: true,
          },
          {
            title: getText('total-amount'),
            key: 'total-amount',
            className: 'text-left',
            formatter: ['amount', 'number'],
            fn: (each): number => each.cost * each.count,
          },
          {
            title: getText('date'),
            key: 'date',
            formatter: ['date', 'number'],
            className: 'text-center',
            sortable: true,
          },
          { title: getText('type'), key: 'type.title', sortable: true },
          { title: getText('description'), key: 'description', sortable: true },
          { title: getText('account'), key: 'account.name', sortable: true },
          { title: getText('entity'), key: 'entity.title', sortable: true },
          {
            title: getText('second-entity'),
            key: 'secondEntity.title',
            sortable: true,
          },
        ]}
        data={data?.costs}
        onSort={onSort}
        sort={sort[0]}
        onEdit={(n): void => {
          setIsOpen(true);
          setEditId(n);
        }}
        onDelete={(): void => {}}
        pagination={{
          page,
          pageSize,
          onPageChange: (_, newPage): void => setPage(newPage),
          onPageSizeChange: (event): void =>
            setPageSize(Number(event.target.value)),
        }}
        loading={apollo.isLoading(networkStatus)}
      />
    </>
  );
};

export default Costs;
