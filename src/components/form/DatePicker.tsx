import React from 'react';
import DatePicker from '@material-ui/lab/DatePicker';
import TextField from '@material-ui/core/TextField';
import { Control, Controller } from 'react-hook-form';

type Props = {
  placeholder?: string;
  name: string;
  control: Control;
};

const Picker = ({ placeholder, name, control }: Props): React.ReactElement => (
  <Controller
    name={name}
    control={control}
    render={({ field: { value, onChange, onBlur } }): React.ReactElement => (
      <DatePicker
        onChange={onChange}
        value={value}
        renderInput={(params): React.ReactElement => (
          // eslint-disable-next-line react/jsx-props-no-spreading
          <TextField {...params} />
        )}
      />
    )}
  />
);

Picker.defaultProps = {
  placeholder: undefined,
};

export default Picker;
