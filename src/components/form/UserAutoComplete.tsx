import React from 'react';
import type { User } from 'types';
import { userFragment } from 'fragments';
import { useQuery } from '@apollo/react-hooks';
import { Control } from 'react-hook-form';
import AutoComplete from './AutoComplete';

type Props = {
  placeholder?: string;
  name: string;
  control: Control;
};

const UserAutoComplete = ({
  placeholder,
  name,
  control,
}: Props): React.ReactElement | null => {
  const { data: users } = useQuery<{
    users: Array<User>;
  }>(userFragment.query);
  return (
    <AutoComplete
      name={name}
      placeholder={placeholder}
      control={control}
      options={users?.users ?? []}
      getOptionLabel={(option: User): string =>
        `${option.firstName} ${option.lastName}`
      }
    />
  );
};

UserAutoComplete.defaultProps = {
  placeholder: undefined,
};

export default UserAutoComplete;
