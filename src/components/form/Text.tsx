import React from 'react';
import isEqual from 'react-fast-compare';
import { Control, Controller } from 'react-hook-form';
import TextField from '@material-ui/core/TextField';

type Props = {
  placeholder?: string;
  name: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  type?: 'password';
  control: Control;
  multiline?: boolean;
};

const Text = ({
  placeholder,
  name,
  type,
  control,
  multiline,
}: Props): React.ReactElement => (
  <Controller
    name={name}
    control={control}
    defaultValue=""
    render={({ field }): React.ReactElement => (
      <TextField
        label={placeholder}
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...field}
        placeholder={placeholder}
        type={type ?? 'text'}
        multiline={multiline}
      />
    )}
  />
);
Text.defaultProps = {
  placeholder: undefined,
  type: 'text',
  multiline: false,
};

export default React.memo(Text, isEqual);
