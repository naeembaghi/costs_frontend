import React from 'react';
import type { AccountType } from 'types';
import { accountTypeFragment } from 'fragments';
import { useQuery } from '@apollo/react-hooks';
import { Control } from 'react-hook-form';
import AutoComplete from './AutoComplete';

type Props = {
  placeholder?: string;
  name: string;
  control: Control;
};

const AccountTypeAutoComplete = ({
  placeholder,
  name,
  control,
}: Props): React.ReactElement | null => {
  const { data: types } = useQuery<{
    accountTypes: Array<AccountType>;
  }>(accountTypeFragment.query);
  return (
    <AutoComplete
      name={name}
      placeholder={placeholder}
      control={control}
      options={types?.accountTypes ?? []}
      getOptionLabel={(option: AccountType): string => option.name}
    />
  );
};

AccountTypeAutoComplete.defaultProps = {
  placeholder: undefined,
};

export default AccountTypeAutoComplete;
