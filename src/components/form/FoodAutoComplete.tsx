import React from 'react';
import type { Food } from 'types';
import { foodFragment } from 'fragments';
import { useQuery } from '@apollo/react-hooks';
import { Control } from 'react-hook-form';
import AutoComplete from './AutoComplete';

type Props = {
  placeholder?: string;
  name: string;
  control: Control;
};

const FoodAutoComplete = ({
  placeholder,
  name,
  control,
}: Props): React.ReactElement | null => {
  const { data: foods } = useQuery<{ foods: Array<Food> }>(foodFragment.query);
  return (
    <AutoComplete
      name={name}
      placeholder={placeholder}
      control={control}
      options={foods?.foods ?? []}
      getOptionLabel={(option): string => option.title}
    />
  );
};

FoodAutoComplete.defaultProps = {
  placeholder: undefined,
};

export default FoodAutoComplete;
