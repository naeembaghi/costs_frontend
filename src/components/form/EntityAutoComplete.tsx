import React from 'react';
import type { Entity } from 'types';
import { entityFragment } from 'fragments';
import { useQuery } from '@apollo/react-hooks';
import { Control } from 'react-hook-form';
import AutoComplete from './AutoComplete';

type Props = {
  placeholder?: string;
  name: string;
  control: Control;
};

const EntityAutoComplete = ({
  placeholder,
  name,
  control,
}: Props): React.ReactElement => {
  const { data: entities } = useQuery<{ entities: Array<Entity> }>(
    entityFragment.query,
  );
  return (
    <AutoComplete
      name={name}
      placeholder={placeholder}
      control={control}
      options={entities?.entities ?? []}
      getOptionLabel={(option): string => option.title}
    />
  );
};

EntityAutoComplete.defaultProps = {
  placeholder: undefined,
};

export default EntityAutoComplete;
