import React from 'react';
import type { Meal } from 'types';
import { mealFragment } from 'fragments';
import { useQuery } from '@apollo/react-hooks';
import { Control } from 'react-hook-form';
import AutoComplete from './AutoComplete';

type Props = {
  placeholder?: string;
  name: string;
  control: Control;
};

const MealAutoComplete = ({
  placeholder,
  name,
  control,
}: Props): React.ReactElement | null => {
  const { data: meals, loading } = useQuery<{ meals: Array<Meal> }>(
    mealFragment.query,
  );
  if (loading || !meals) return null;
  return (
    <AutoComplete
      name={name}
      placeholder={placeholder}
      control={control}
      options={meals?.meals ?? []}
      getOptionLabel={(option): string => option.title}
    />
  );
};

MealAutoComplete.defaultProps = {
  placeholder: undefined,
};

export default MealAutoComplete;
