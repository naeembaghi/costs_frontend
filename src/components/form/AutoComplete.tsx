import React from 'react';
import MuiAutoComplete from '@material-ui/core/Autocomplete';
import TextField from '@material-ui/core/TextField';
import { Control, Controller } from 'react-hook-form';

type Props<T> = {
  placeholder?: string;
  name: string;
  control: Control;
  getOptionLabel: (option: T) => string;
  options: Array<T>;
};

const AutoComplete = <T extends unknown>({
  placeholder,
  name,
  control,
  getOptionLabel,
  options,
}: Props<T>): React.ReactElement => (
  <div className="flex items-center date-picker-container">
    <Controller
      name={name}
      control={control}
      render={({ field: { value, onChange, onBlur } }): React.ReactElement => (
        <MuiAutoComplete
          placeholder={placeholder}
          renderInput={(params): React.ReactElement => (
            <TextField
              // eslint-disable-next-line react/jsx-props-no-spreading
              {...params}
              name={name}
              label={placeholder}
              placeholder={placeholder}
            />
          )}
          options={options}
          defaultValue={value}
          getOptionLabel={getOptionLabel}
          onChange={(event, val): void => onChange(val)}
        />
      )}
    />
  </div>
);

AutoComplete.defaultProps = {
  placeholder: undefined,
};

export default AutoComplete;
