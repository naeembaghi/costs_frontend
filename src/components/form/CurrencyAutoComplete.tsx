import React from 'react';
import type { Currency } from 'types';
import { currencyFragment } from 'fragments';
import { useQuery } from '@apollo/react-hooks';
import { Control } from 'react-hook-form';
import AutoComplete from './AutoComplete';

type Props = {
  placeholder?: string;
  name: string;
  control: Control;
};

const CurrencyAutoComplete = ({
  placeholder,
  name,
  control,
}: Props): React.ReactElement | null => {
  const { data: currencies } = useQuery<{
    currencies: Array<Currency>;
  }>(currencyFragment.query);
  return (
    <AutoComplete
      name={name}
      placeholder={placeholder}
      control={control}
      options={currencies?.currencies ?? []}
      getOptionLabel={(option: Currency): string =>
        `${option.name} - ${option.symbol}`
      }
    />
  );
};

CurrencyAutoComplete.defaultProps = {
  placeholder: undefined,
};

export default CurrencyAutoComplete;
