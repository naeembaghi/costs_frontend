import React from 'react';
import type { Account } from 'types';
import { accountFragment } from 'fragments';
import { useQuery } from '@apollo/react-hooks';
import { Control } from 'react-hook-form';
import AutoComplete from './AutoComplete';

type Props = {
  placeholder?: string;
  name: string;
  control: Control;
};

const AccountAutoComplete = ({
  placeholder,
  name,
  control,
}: Props): React.ReactElement => {
  const { data: accounts } = useQuery<{ accounts: Array<Account> }>(
    accountFragment.query,
  );
  return (
    <AutoComplete<Account>
      name={name}
      control={control}
      placeholder={placeholder}
      options={accounts?.accounts ?? []}
      getOptionLabel={(option): string =>
        `${option.name || ''} - ${option.accountNo || ''} - ${
          option.currency?.name
        }`
      }
    />
  );
};

AccountAutoComplete.defaultProps = {
  placeholder: undefined,
};

export default AccountAutoComplete;
