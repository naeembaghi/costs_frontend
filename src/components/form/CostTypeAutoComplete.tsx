import React from 'react';
import type { Type } from 'types';
import { typeFragment } from 'fragments';
import { useQuery } from '@apollo/react-hooks';
import { Control } from 'react-hook-form';
import AutoComplete from './AutoComplete';

type Props = {
  placeholder?: string;
  name: string;
  control: Control;
  initialValue?: Type;
};

const CostTypeAutoComplete = ({
  placeholder,
  name,
  control,
  initialValue,
}: Props): React.ReactElement | null => {
  const { data: types } = useQuery<{ types: Array<Type> }>(typeFragment.query);
  return (
    <AutoComplete<Type>
      name={name}
      placeholder={placeholder}
      control={control}
      options={types?.types ?? []}
      getOptionLabel={(option): string => option.title}
    />
  );
};

CostTypeAutoComplete.defaultProps = {
  placeholder: undefined,
  initialValue: undefined,
};

export default CostTypeAutoComplete;
