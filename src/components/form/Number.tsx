import { TextField } from '@material-ui/core';
import React from 'react';
import { Control, Controller } from 'react-hook-form';

type Props = {
  placeholder?: string;
  name: string;
  control: Control;
};

const Number = ({ placeholder, name, control }: Props): React.ReactElement => (
  <Controller
    name={name}
    control={control}
    render={({ field }): React.ReactElement => (
      <TextField
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...field}
        inputProps={{ step: '0.001' }}
        placeholder={placeholder}
        label={placeholder}
        type="number"
        className="w-full"
      />
    )}
  />
);

Number.defaultProps = {
  placeholder: undefined,
};

export default Number;
