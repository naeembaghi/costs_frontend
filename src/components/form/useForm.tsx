import React from 'react';
import useLocalizer from 'localizer';
import { useForm as useReactHookForm } from 'react-hook-form';
import { Button } from '@material-ui/core';
import Text from './Text';
import Number from './Number';
import DatePicker from './DatePicker';
import CostTypeAutoComplete from './CostTypeAutoComplete';
import AccountAutoComplete from './AccountAutoComplete';
import EntityAutoComplete from './EntityAutoComplete';
import AccountTypeAutoComplete from './AccountTypeAutoComplete';
import UserAutoComplete from './UserAutoComplete';
import CurrencyAutoComplete from './CurrencyAutoComplete';
import FoodAutoComplete from './FoodAutoComplete';
import MealAutoComplete from './MealAutoComplete';

type Props<T> = {
  fields: Array<
    | {
        type: 'text' | 'textarea' | 'autocomplete' | 'password';
        name: string;
        placeholder?: string;
      }
    | {
        type: 'number';
        name: string;
        placeholder?: string;
      }
    | {
        type: 'date';
        name: string;
        placeholder?: string;
        timePicker: boolean;
      }
    | {
        type:
          | 'cost-type-autocomplete'
          | 'account-autocomplete'
          | 'entity-autocomplete'
          | 'account-type-autocomplete'
          | 'currency-autocomplete'
          | 'food-autocomplete'
          | 'meal-autocomplete'
          | 'user-autocomplete';
        name: string;
        placeholder?: string;
      }
  >;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  onSubmit: (data: T) => void;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  defaultValues: Partial<T>;
  onCancel?: () => void;
};

type FormProps<T> = {
  model?: T;
};

const useForm = <T extends unknown>({
  fields,
  onSubmit,
  defaultValues,
  onCancel,
}: Props<T>): { Form: React.ComponentType<FormProps<T>> } => {
  const { getText } = useLocalizer();
  const Form = ({ model }: FormProps<T>): React.ReactElement => {
    const { handleSubmit, control } = useReactHookForm({
      defaultValues: model ?? defaultValues,
    });
    return (
      <form
        className="grid grid-cols-1 gap-6"
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        onSubmit={handleSubmit(onSubmit)}
      >
        {fields.map((each) => {
          switch (each.type) {
            case 'text':
              return (
                <Text
                  key={each.name}
                  placeholder={each.placeholder ?? getText(each.name)}
                  name={each.name}
                  control={control}
                />
              );
            case 'password':
              return (
                <Text
                  key={each.name}
                  placeholder={each.placeholder ?? getText(each.name)}
                  name={each.name}
                  control={control}
                  type="password"
                />
              );
            case 'textarea':
              return (
                <Text
                  key={each.name}
                  placeholder={each.placeholder ?? getText(each.name)}
                  name={each.name}
                  control={control}
                  multiline
                />
              );
            case 'number':
              return (
                <Number
                  key={each.name}
                  placeholder={each.placeholder ?? getText(each.name)}
                  name={each.name}
                  control={control}
                />
              );
            case 'date':
              return (
                <DatePicker
                  key={each.name}
                  name={each.name}
                  placeholder={each.placeholder ?? getText(each.name)}
                  control={control}
                />
              );
            case 'cost-type-autocomplete':
              return (
                <CostTypeAutoComplete
                  key={each.name}
                  placeholder={each.placeholder ?? getText(each.name)}
                  name={each.name}
                  control={control}
                />
              );
            case 'account-autocomplete':
              return (
                <AccountAutoComplete
                  key={each.name}
                  placeholder={each.placeholder ?? getText(each.name)}
                  name={each.name}
                  control={control}
                />
              );
            case 'entity-autocomplete':
              return (
                <EntityAutoComplete
                  key={each.name}
                  placeholder={each.placeholder ?? getText(each.name)}
                  name={each.name}
                  control={control}
                />
              );
            case 'account-type-autocomplete':
              return (
                <AccountTypeAutoComplete
                  key={each.name}
                  placeholder={each.placeholder ?? getText(each.name)}
                  name={each.name}
                  control={control}
                />
              );
            case 'currency-autocomplete':
              return (
                <CurrencyAutoComplete
                  key={each.name}
                  placeholder={each.placeholder ?? getText(each.name)}
                  name={each.name}
                  control={control}
                />
              );
            case 'user-autocomplete':
              return (
                <UserAutoComplete
                  key={each.name}
                  placeholder={each.placeholder ?? getText(each.name)}
                  name={each.name}
                  control={control}
                />
              );
            case 'food-autocomplete':
              return (
                <FoodAutoComplete
                  key={each.name}
                  placeholder={each.placeholder ?? getText(each.name)}
                  name={each.name}
                  control={control}
                />
              );
            case 'meal-autocomplete':
              return (
                <MealAutoComplete
                  key={each.name}
                  placeholder={each.placeholder ?? getText(each.name)}
                  name={each.name}
                  control={control}
                />
              );
          }
          return null;
        })}
        <div className="flex w-full">
          <Button
            type="submit"
            variant="contained"
            color="primary"
            className="flex-1"
          >
            {getText('submit')}
          </Button>
          {onCancel && (
            <>
              <div className="w-4" />
              <Button
                variant="contained"
                color="secondary"
                onClick={onCancel}
                className="flex-1"
              >
                {getText('cancel')}
              </Button>
            </>
          )}
        </div>
      </form>
    );
  };
  Form.defaultProps = {
    model: undefined,
  };
  return { Form };
};

useForm.defaultProps = {
  onCancel: undefined,
};

export default useForm;
