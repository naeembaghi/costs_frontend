import React from 'react';
import 'react-responsive-modal/styles.css';
import './App.generated.css';
import { useSelector } from 'react-redux';
import Wrapper from 'components/wrapper';
import Router, { LoginRouter } from 'router';
import type { Store } from './redux/types/Store';

const App = (): React.ReactElement => {
  const token = useSelector((state: Store) => state.session?.token);
  if (!token) return <LoginRouter />;
  return (
    <Wrapper>
      <Router />
    </Wrapper>
  );
};

export default App;
