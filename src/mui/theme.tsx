import React from 'react';
import { ThemeProvider, createTheme } from '@material-ui/core/styles';
import { StylesProvider, jssPreset } from '@material-ui/styles';
import { create } from 'jss';
import rtl from 'jss-rtl';
import LocalizationProvider from '@material-ui/lab/LocalizationProvider';
import AdapterJalali from '@date-io/date-fns-jalali';
import purple from '@material-ui/core/colors/blue';
import orange from '@material-ui/core/colors/orange';

// Configure JSS
const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

const theme = createTheme({
  direction: 'rtl',
  palette: {
    primary: {
      main: purple[500],
    },
    secondary: {
      main: orange[300],
    },
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          color: '#ffffff',
        },
        outlined: { color: '#000000' },
      },
    },
    MuiListItem: {
      styleOverrides: {
        root: {
          textAlign: 'right',
        },
      },
    },
    MuiTableCell: {
      styleOverrides: {
        root: {
          textAlign: 'right',
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          textAlign: 'right',
        },
        notchedOutline: {
          textAlign: 'right',
        },
      },
    },
    MuiInputLabel: {
      styleOverrides: {
        root: {
          textAlign: 'right',
          left: 'initial',
          transformOrigin: 'top right',
          transform: 'translate(-14px, 16px) scale(1)',
        },
        shrink: {
          left: 'initial',
          right: 0,
          transformOrigin: 'top right',
          transform: 'translate(-14px, -9px) scale(0.75)',
        },
      },
    },
    MuiAutocomplete: {
      styleOverrides: {
        endAdornment: {
          right: 'initial !important',
          left: 9,
        },
        inputRoot: {
          paddingRight: '9px !important',
          paddingLeft: 65,
        },
      },
    },
  },
  typography: {
    fontFamily: ['Vazir'].join(','),
    button: {},
  },
});

const Provider = ({
  children,
}: {
  children: React.ReactChild;
}): React.ReactElement => (
  <ThemeProvider theme={theme}>
    <LocalizationProvider dateAdapter={AdapterJalali}>
      <StylesProvider jss={jss}>{children}</StylesProvider>
    </LocalizationProvider>
  </ThemeProvider>
);

export default Provider;
