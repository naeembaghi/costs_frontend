// unauthenticated routes
export const LOGIN = '/login';
export const SIGN_UP = '/sign-up';

// authenticated routes
export const ACCOUNTS = '/accounts';
export const MEALS = '/meals';
export const DAILY_MEALS = '/daily_meals';
export const ENTITIES = '/entities';
export const COSTS = '/costs';
export const TYPES = '/types';
export const DEBTS_VIEW = '/debts_view';
export const DEBTS = '/debts';
export const PAYMENTS = '/payments';
export const FOODS = '/foods';
export const COSTS_REPORT = '/costs_report';
export const COSTS_TYPES_REPORT = '/costs_types_report';
