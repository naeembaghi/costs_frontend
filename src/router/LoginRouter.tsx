import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import Login from 'screens/login';
import SignUp from 'screens/sign-up';

import { SIGN_UP } from './routes';

const LoginRouter = (): React.ReactElement => (
  <BrowserRouter>
    <Switch>
      <Route path={SIGN_UP}>
        <SignUp />
      </Route>
      <Route>
        <Login />
      </Route>
    </Switch>
  </BrowserRouter>
);

export default LoginRouter;
