import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Lobby from 'components/lobby';
import Meals from 'components/meals';
import Accounts from 'components/accounts';
import DailyMeals from 'components/dailyMeals';
import Entities from 'components/entities';
import Costs from 'components/costs';
import Types from 'components/types';
import DebtsView from 'components/debtsView';
import Debts from 'components/debts';
import Payments from 'components/payments';
import Foods from 'components/foods';
import CostsReport from 'screens/costsReport';
import CostsTypeReport from 'screens/costsTypeReport';
import {
  ACCOUNTS,
  DEBTS_VIEW,
  ENTITIES,
  MEALS,
  DAILY_MEALS,
  TYPES,
  COSTS,
  DEBTS,
  PAYMENTS,
  FOODS,
  COSTS_REPORT,
  COSTS_TYPES_REPORT,
} from './routes';

const Router = (): React.ReactElement => (
  <Switch>
    <Route path={DEBTS}>
      <Debts />
    </Route>
    <Route path={PAYMENTS}>
      <Payments />
    </Route>
    <Route path={DEBTS_VIEW}>
      <DebtsView />
    </Route>
    <Route path={COSTS}>
      <Costs />
    </Route>
    <Route path={TYPES}>
      <Types />
    </Route>
    <Route path={ENTITIES}>
      <Entities />
    </Route>
    <Route path={ACCOUNTS}>
      <Accounts />
    </Route>
    <Route path={DAILY_MEALS}>
      <DailyMeals />
    </Route>
    <Route path={MEALS}>
      <Meals />
    </Route>
    <Route path={FOODS}>
      <Foods />
    </Route>
    <Route path={COSTS_REPORT}>
      <CostsReport />
    </Route>
    <Route path={COSTS_TYPES_REPORT}>
      <CostsTypeReport />
    </Route>
    <Route path="/">
      <Lobby />
    </Route>
  </Switch>
);

export default Router;
