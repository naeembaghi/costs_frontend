const colors = [
  '#DC2626',
  '#D97706',
  '#059669',
  '#2563EB',
  '#4F46E5',
  '#7C3AED',
  '#DB2777',
  '#65A30D',
  '#2563EB',
  '#C026D3',
  '#DB2777',
  '#713F12',
];

const useRandomColor = (): (() => string) => (): string => {
  const random = Math.floor(Math.random() * colors.length);
  return colors[random];
};

export default useRandomColor;
