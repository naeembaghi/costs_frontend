import resolveConfig from 'tailwindcss/resolveConfig';
/* eslint-disable */
// @ts-ignore
import config from '../tailwind.config.js';
/* eslint-enable */

const useTheme = (): {
  colors: { primary: string };
  screens: Record<string, string>;
} => {
  const fullConfig = resolveConfig(config);
  return fullConfig.theme;
};

export default useTheme;
