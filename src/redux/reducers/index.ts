import { combineReducers } from 'redux';
import login from './login/login';
import ui from './ui';

export default combineReducers({ session: login, ui });
