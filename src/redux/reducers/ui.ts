import { TOGGLE_DRAWER } from 'redux/actions/ui/actionTypes';

const initialState = {};

type StateType = {
  showDrawer?: boolean;
};

type ActionType = {
  type: string;
};

const session = (
  state: StateType = initialState,
  action: ActionType,
): StateType => {
  switch (action.type) {
    case TOGGLE_DRAWER: {
      return {
        ...state,
        showDrawer: !state.showDrawer,
      };
    }
    default:
      return state;
  }
};

export default session;
