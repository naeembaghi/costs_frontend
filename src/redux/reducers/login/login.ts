import { LOGIN, LOGOUT } from 'redux/actions/login/actionTypes';

const initialState = {};

type StateType = {
  token?: string;
};

type ActionType = {
  type: string;
  payload: { token?: string };
};

const session = (
  state: StateType = initialState,
  action: ActionType,
): StateType => {
  switch (action.type) {
    case LOGIN: {
      const { token } = action.payload;
      return {
        ...state,
        token,
      };
    }
    case LOGOUT: {
      return {
        ...state,
        token: undefined,
      };
    }
    default:
      return state;
  }
};

export default session;
