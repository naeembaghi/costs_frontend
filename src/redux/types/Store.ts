export type Store = {
  session?: {
    token?: string;
  };
  ui?: {
    showDrawer: boolean;
  };
};
