import { LOGIN, LOGOUT } from './actionTypes';

export const login = (
  token: string,
): { type: string; payload: { token: string } } => ({
  type: LOGIN,
  payload: { token },
});

export const logout = (): { type: string } => ({
  type: LOGOUT,
});

const defaultExport = null;
export default defaultExport;
