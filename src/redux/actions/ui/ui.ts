import { TOGGLE_DRAWER } from './actionTypes';

export const toggleDrawer = (): { type: string } => ({
  type: TOGGLE_DRAWER,
});

const defaultExport = null;
export default defaultExport;
