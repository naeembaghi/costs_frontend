import { createStore, applyMiddleware } from 'redux';
// eslint-disable-next-line import/no-extraneous-dependencies
import logger from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web
import rootReducers from './reducers';

const persistConfig = {
  key: 'root',
  storage,
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const persistedReducer: any = persistReducer(persistConfig, rootReducers);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const store: any = createStore(
  persistedReducer,
  applyMiddleware(logger),
);
export const persistor = persistStore(store);
